package com.prueba.intercorp.ui.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import com.google.gson.annotations.SerializedName

@Parcelize
data class Client(
    @SerializedName("id")
    val Id : String = "",
    @SerializedName("nombre")
    val Nombre : String = "",
    @SerializedName("apellido")
    val Apellido : String = "",
    @SerializedName("edad")
    val Edad : Int = 0,
    @SerializedName("fechaNac")
    val FechaNac : String = ""
) : Parcelable

data class ClientList(
    val clientList: List<Client> = listOf()
)