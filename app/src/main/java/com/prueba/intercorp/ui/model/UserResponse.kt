package com.prueba.intercorp.ui.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserResponse (
    val id: Int = 0,
    val token: String = "",
    val name: String = "",
    val surName : String = "",
    val edad : Int = 0,
    val fechaNac : String = ""
) : Parcelable