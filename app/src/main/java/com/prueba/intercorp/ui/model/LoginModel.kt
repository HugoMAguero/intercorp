package com.prueba.intercorp.ui.model

data class CustomModel (
    val user: Int = 0,
    val password: String = ""
)

data class FacebookModel (
    val user: Int = 0,
    val password: String = ""
)
