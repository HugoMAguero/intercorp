package com.prueba.intercorp.ui.fragment

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.prueba.intercorp.R
import com.prueba.intercorp.data.DataSource
import com.prueba.intercorp.domain.RepoImplement
import com.prueba.intercorp.ui.model.CustomModel
import com.prueba.intercorp.ui.model.FacebookModel
import com.prueba.intercorp.viewModel.LoginViewModel
import com.prueba.intercorp.viewModel.VMFactory
import com.prueba.intercorp.vo.Resource
import kotlinx.android.synthetic.main.fragment_login.*
import java.lang.Integer.parseInt


class LoginFragment : Fragment() {

    private var callbackManager: CallbackManager = CallbackManager.Factory.create()

    private val viewModel by viewModels<LoginViewModel> {
        VMFactory(
            RepoImplement(DataSource())
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginCustom()
        loginFacebook()
    }

    private fun loginCustom() {
        btn_login.setOnClickListener(View.OnClickListener {
            val userCustom = parseInt(userLogin.text.toString())
            val passwordCustom = passLogin.text.toString()
            val userLogin = CustomModel(userCustom, passwordCustom)
            viewModel.login(userLogin).observe(viewLifecycleOwner, Observer { result ->
                when (result) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                    }
                    is Resource.Failure -> {
                        Toast.makeText(context, "Usuario y/o Contraseña inválidos", Toast.LENGTH_SHORT).show()
                    }
                }
            })
        })
    }

    private fun loginFacebook() {
        val accessToken: AccessToken = AccessToken.getCurrentAccessToken()
        if(accessToken != null && !accessToken.isExpired()) {
            fetchFacebookUser(accessToken.token)
        } else {
            login_Facebook.setReadPermissions("email")
            LoginManager.getInstance().logInWithReadPermissions(activity, listOf("email"))
            LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult?) {
                        loginResult?.let {
                            val token = it.accessToken.token
                            val credential = FacebookAuthProvider.getCredential(token)
                            FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener {
                                if (it.isSuccessful) {
                                    fetchFacebookUser(parseInt(it.result.user.phoneNumber!!))
                                }
                            }
                        }
                    }

                    override fun onCancel() {
                    }

                    override fun onError(exception: FacebookException) {
                        Toast.makeText(context, "error iniciando sesión.", Toast.LENGTH_SHORT).show()
                    }
                })
        }
    }

    private fun fetchFacebookUser(user: Int) {
        val facebookLogin = FacebookModel(user,"")
        viewModel.loginWithFacebook(facebookLogin).observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
                }
                is Resource.Success -> {
                    findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                }
                is Resource.Failure -> {
                    Toast.makeText(context, "Error iniciando sesión", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}