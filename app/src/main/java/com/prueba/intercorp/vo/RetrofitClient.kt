package com.prueba.intercorp.vo

import com.prueba.intercorp.domain.WebService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    val webService by lazy{
        Retrofit.Builder()
            .baseUrl("https://intercorp.com/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(WebService::class.java)
    }
}