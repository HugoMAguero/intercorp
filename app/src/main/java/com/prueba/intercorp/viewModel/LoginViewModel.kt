package com.prueba.intercorp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.prueba.intercorp.domain.Repo
import com.prueba.intercorp.ui.model.CustomModel
import com.prueba.intercorp.ui.model.FacebookModel
import com.prueba.intercorp.vo.Resource
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class LoginViewModel (private val repository: Repo): ViewModel()  {

    fun login(userLogin: CustomModel) = liveData(Dispatchers.IO){
        emit(Resource.Loading())
        try {
            emit(repository.login(userLogin))
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }
    }

    fun loginWithFacebook(userLogin: FacebookModel) = liveData(Dispatchers.IO){
        emit(Resource.Loading())
        try {
            emit(repository.loginWithFacebook(userLogin))
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }
    }
}