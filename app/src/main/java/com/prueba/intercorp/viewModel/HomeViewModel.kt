package com.prueba.intercorp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.prueba.intercorp.domain.Repo
import com.prueba.intercorp.vo.Resource
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class HomeViewModel (private val repo: Repo) : ViewModel() {

    fun getClientList() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(repo.getClientList())
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }
    }
}