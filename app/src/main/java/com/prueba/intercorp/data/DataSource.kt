package com.prueba.intercorp.data

import com.prueba.intercorp.ui.model.Client
import com.prueba.intercorp.ui.model.CustomModel
import com.prueba.intercorp.ui.model.FacebookModel
import com.prueba.intercorp.ui.model.UserResponse
import com.prueba.intercorp.vo.Resource
import com.prueba.intercorp.vo.RetrofitClient

class DataSource() {

    suspend fun login(userLogin: CustomModel): Resource<UserResponse> {
        return Resource.Success(RetrofitClient.webService.login(userLogin))
    }

    suspend fun loginFacebook(userLogin: FacebookModel): Resource<UserResponse> {
        return Resource.Success(RetrofitClient.webService.loginWithFacebook(userLogin))
    }

    suspend fun getClientList(): Resource<List<Client>> {
        return Resource.Success(RetrofitClient.webService.getClientList().clientList)
    }
}