package com.prueba.intercorp.domain

import com.prueba.intercorp.ui.model.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface WebService {
    @POST("account/login/custom")
    suspend fun login(@Body userLogin: CustomModel): UserResponse

    @POST("account/login/facebook")
    suspend fun loginWithFacebook(@Body userLogin: FacebookModel): UserResponse

    @GET("filter.php")
    suspend fun getClientList() : ClientList
}