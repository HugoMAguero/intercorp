package com.prueba.intercorp.domain

import com.prueba.intercorp.data.DataSource
import com.prueba.intercorp.ui.model.Client
import com.prueba.intercorp.ui.model.CustomModel
import com.prueba.intercorp.ui.model.FacebookModel
import com.prueba.intercorp.ui.model.UserResponse
import com.prueba.intercorp.vo.Resource

class RepoImplement(private val dataSource: DataSource) : Repo{

    override suspend fun login(userLogin: CustomModel): Resource<UserResponse> {
        return dataSource.login(userLogin)
    }

    override suspend fun loginWithFacebook(userLogin: FacebookModel): Resource<UserResponse> {
        return dataSource.loginFacebook(userLogin)
    }

    override suspend fun getClientList(): Resource<List<Client>> {
        return dataSource.getClientList()
    }
}