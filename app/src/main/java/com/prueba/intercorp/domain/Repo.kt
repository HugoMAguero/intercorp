package com.prueba.intercorp.domain

import com.prueba.intercorp.ui.model.Client
import com.prueba.intercorp.ui.model.CustomModel
import com.prueba.intercorp.ui.model.FacebookModel
import com.prueba.intercorp.ui.model.UserResponse
import com.prueba.intercorp.vo.Resource

interface Repo {
    suspend fun login(user : CustomModel) : Resource<UserResponse>

    suspend fun loginWithFacebook(user : FacebookModel) : Resource<List<Client>>

    suspend fun getClientList() : Resource<List<Client>>
}